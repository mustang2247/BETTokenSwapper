#include <utility>
#include <string>
#include <eosiolib/eosio.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/contract.hpp>
#include <eosiolib/types.hpp>
#include <eosiolib/transaction.hpp>

using eosio::asset;
using eosio::permission_level;
using eosio::action;
using eosio::name;
using eosio::unpack_action_data;
using eosio::symbol_type;
using eosio::transaction;


class BETTokenSwapper : public eosio::contract {
	public:
		// minimum of 1000.0000 EOS
		const uint64_t MIN_EOS_TO_SEND = 10000000;
		// number of BET to transfer out, 5 BET per EOS
		const uint64_t BET_PER_EOS_times10 = 240;

		// initialize contract
		BETTokenSwapper(account_name self):eosio::contract(self){}

		// handles all eos.io token transfers
		void transfer(uint64_t sender, uint64_t receiver){

			auto transfer_data = unpack_action_data<st_transfer>();

			// only trigger on transfers to contract, not transfers from contract
			if (transfer_data.from == _self){
				return;
			}

			// require that the sender of the transfer is 
			eosio_assert(transfer_data.quantity.symbol == symbol_type(S(4, EOS)), "You must only transfer EOS.");
			eosio_assert(transfer_data.quantity.is_valid(), "Invalid asset.");

			uint64_t your_transfer_amt = (uint64_t)transfer_data.quantity.amount;

			eosio_assert(your_transfer_amt >= MIN_EOS_TO_SEND, "You did not send enough EOS. Minimum 1000.");

			uint64_t your_bet_amt = (your_transfer_amt * BET_PER_EOS_times10) / 10;
			uint64_t ref_bet_amt = 0;

			// if the name is a referral
			const account_name possible_ref = eosio::string_to_name(transfer_data.memo.c_str());

			if (possible_ref != _self && possible_ref != transfer_data.from && is_account(possible_ref)){
				ref_bet_amt = your_bet_amt / 100;
				your_bet_amt = (101 * your_bet_amt) / 100;
				

				transaction ref_tx{};
				ref_tx.actions.emplace_back(
					permission_level{_self, N(active)}, 
					N(betdividends), 
					N(transfer), 
					std::make_tuple(
						_self, 
						possible_ref, 
						asset(ref_bet_amt, symbol_type(S(4, BET))), 
						std::string("Pre-sale referral reward!")
					)
				);
				ref_tx.delay_sec = 5;
				ref_tx.send(possible_ref, _self);
			}


			// now send tokens to the INVESTOR

			// if this function fails (cannot send tokens), then the entire transfer will fail
			// INVESTOR will receive EOS back into account as if nothing happened.
			action(
				permission_level({_self, N(active)}),
				N(betdividends),
				N(transfer),
				std::make_tuple(
					_self, 
					transfer_data.from, 
					asset(your_bet_amt, symbol_type(S(4, BET))), 
					std::string("Thank you for your investment!")
				)
			).send();

			// now send all the EOS to the EOSBet Casino address
			// this action will also fail if the transfer does not go through.
			action(
				permission_level({_self, N(active)}),
				N(eosio.token),
				N(transfer),
				std::make_tuple(_self, 
					N(eosbetcasino), 
					asset(your_transfer_amt, symbol_type(S(4, EOS))), 
					std::string("")
				)
			).send();

			// Transfer is complete.

			// Details about contract:

			// Basically, I will "hard code" the amount of EOS to send, and the BET token you will get, as well as your "sending" address
			// I will then seed the contract with the correct amount of BET token
				// if I do not do this, and you send EOS, the entire transaction will fail. No EOS or BET will be sent
			// Then you will send the EOS to the contract.
			// This transaction will automatically trigger the above transfer(...) function
			// That function will ATOMICALLY transfer the BET to INVESTOR, and then the EOS to the EOSBETCASINO wallet.
			// ATOMIC means that if any part of the transaction fails, then the entire transaction will fail.
		}

	private:

		// taken from eosio.token.hpp
		// structure of a standard token transfer
		struct st_transfer {
            account_name  from;
            account_name  to;
            asset         quantity;
            std::string   memo;
         };
};

#define EOSIO_ABI_EX( TYPE, MEMBERS ) \
extern "C" { \
   void apply( uint64_t receiver, uint64_t code, uint64_t action ) { \
      auto self = receiver; \
      if( action == N(onerror)) { \
         /* onerror is only valid if it is for the "eosio" code account and authorized by "eosio"'s "active permission */ \
         eosio_assert(code == N(eosio), "onerror action's are only valid from the \"eosio\" system account"); \
      } \
      /* Require that we are calling an action */\
      if( code == self || code == N(eosio.token) || action == N(onerror) ) { \
         TYPE thiscontract( self ); \
         switch( action ) { \
            EOSIO_API( TYPE, MEMBERS ) \
         } \
         /* does not allow destructor of thiscontract to run: eosio_exit(0); */ \
      } \
   } \
}

EOSIO_ABI_EX(BETTokenSwapper,
	(transfer)
)